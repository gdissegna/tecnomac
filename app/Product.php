<?php

namespace App;

use App\Http\Requests\Request;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * Class Product
 * @package App
 */
class Product extends Model implements SluggableInterface
{
    use Translatable;
    use SluggableTrait;

    const PRIMARY_PATH = 'images/products/';
    const FILES_PATH = 'files/';

    protected $fillable = ['name', 'primary_img', 'link', 'description','category_id','inHome','video','pdf'];
    public $translatedAttributes = ['description'];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
    );

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->primary_img != null) {
                if (\File::isFile($model->primary_img)) {
                    \File::delete($model->primary_img);
                }
            }
            if ($model->pdf != null) {
                if (\File::isFile($model->pdf)) {
                    \File::delete($model->pdf);
                }
            }
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function createProduct(Request $request)
    {
        if ($request->hasFile('image')) {
            $imagepath = $this->saveImage($request['image']);
        } else {
            $imagepath = $this->primary_img;
        }
        if ($request->has('pdf')) {
            $pdfpath = $this->savePdf($request['pdf']);
        } else {
            $pdfpath = $this->pdf;
        }

        $data = [
            'primary_img' => $imagepath,
            'pdf' => $pdfpath,
            'name' => $request['name'],
            'inHome' => $request->input('inhome','0'),
            'category_id' => $request['category'],
            'video' => $request['video'],
            'it' => ['description' => $request['description_it']],
            'en' => ['description' => $request['description_en']],
            'de' => ['description' => $request['description_de']],
            'ru' => ['description' => $request['description_ru']]
        ];

        $this->create($data);

        return $this;
    }


    /**
     * test
     * @param Request $request
     * @return $this
     */
    public function updateProduct(Request $request)
    {
        if ($request->hasFile('image')) {
            $this->deleteImage();
            $newimage = $this->saveImage($request['image']);
        } else {
            $newimage = $this->primary_img;
        }
        if ($request->hasFile('pdf')) {
            $this->deletePdf();
            $newpdf = $this->savePdf($request['pdf']);
        } else {
            $newpdf = $this->pdf;
        }

        $data = [
            'primary_img' => $newimage,
            'pdf' => $newpdf,
            'name' => $request['name'],
            'video' => $request['video'],
            'inHome' => $request->input('inhome','0'),
            'category_id' => $request['category'],
            'it' => ['description' => $request['description_it']],
            'en' => ['description' => $request['description_en']],
            'de' => ['description' => $request['description_de']],
            'ru' => ['description' => $request['description_ru']]

        ];

        $this->fill($data)->save();

        return $this;
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    protected function saveImage(UploadedFile $image)
    {

        $filename = $image->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = self::PRIMARY_PATH . $newfilename ;
        Image::make($image)->save($filepath);
        return $filepath;
    }

    /**
     * @param UploadedFile $pdf
     * @return string
     */
    protected function savePdf(UploadedFile $pdf)
    {
        $filename = $pdf->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = public_path() ."/". self::FILES_PATH;
        $pdf->move($filepath,$newfilename);

        $filepath = $filepath . $newfilename;

        return $filepath;
    }

    /**
     * Delete the image file
     */
    public function deleteImage()
    {
        if ($this->image != null) {
            if (\File::isFile($this->image)) {
                \File::delete($this->image);
            }
        }
    }

    /**
     * Delete pdf
     */
    protected function deletePdf()
    {
        if ($this->pdf != null) {
            if (\File::isFile($this->pdf)) {
                \File::delete($this->pdf);
            }
        }
    }


}
