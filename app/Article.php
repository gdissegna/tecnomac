<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Article extends Model
{
    use \Dimsav\Translatable\Translatable;

    const IMAGE_FOLDER = 'images/article/';
    protected $newimage;

    protected $guarded = ['id'];
    protected $fillable = ['title', 'body', 'primary_img', 'slug'];
    public $translatedAttributes = ['title', 'body', 'slug'];

    public function Images()
    {
        return $this->asMany('App\ArticleImage');
    }


    public function saveArticle(Request $request)
    {
        $this->newimage = $this->saveprimaryImage($request['image']);
        $data = [
            'primary_img' => $this->newimage,
            'it' => ['title' => $request['title_it'],  'body' => $request['body_it']],
            'en' => ['title' => $request['title_en'], 'body' => $request['body_en']],
            'de' => ['title' => $request['title_de'], 'body' => $request['body_de']],
            'ru' => ['title' => $request['title_ru'], 'body' => $request['body_ru']],
        ];

        $this->create($data);

    }

    /**
     * @param Request $request
     */
    public function updateArticle(Request $request)
    {
        if ($request->hasFile('image')) {
            $this->deleteImage($this->primary_img);
            $this->newimage = $this->saveprimaryImage($request['image']);
        }

        $data = [
            'primary_img' => $this->newimage,
            'it' => ['title' => $request['title_it'], 'body' => $request['body_it']],
            'en' => ['title' => $request['title_en'], 'body' => $request['body_en']],
            'de' => ['title' => $request['title_de'], 'body' => $request['body_de']],
            'ru' => ['title' => $request['title_ru'], 'body' => $request['body_ru']],
        ];

        $this->fill($data)->save();
    }

    public function deleteArticle()
    {

        $this->deleteImage($this->image);
        $this->delete();
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    public function saveprimaryImage(UploadedFile $image)
    {

        $imagename = time() . $image->getClientOriginalName();
        $imagepath = self::IMAGE_FOLDER . $imagename;
        Image::make($image)->fit(1000, 500, function ($constraint) {
            $constraint->upsize();
        })->save($imagepath);

        return $imagepath;
    }

    public function deleteImage($filepath)
    {
        if (File::isFile($filepath)) {
            File::delete($filepath);
        }
    }

}
