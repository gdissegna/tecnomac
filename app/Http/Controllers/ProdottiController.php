<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryTranslation;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class ProdottiController
 * @property CategoryTranslation translations
 * @package App\Http\Controllers
 */
class ProdottiController extends Controller
{

    protected $translations;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($slug)
    {
        $this->translations = new CategoryTranslation;

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $category = Category::with('products')->first($translation->category_id));

        dd($category);

        return view('prodotti')->with('category',$category);
    }

    /**
     * @param $slug
     * @param $name
     * @return view
     */
    public function prodotto($slug,$name)
    {
        $category = Category::where('slug',$slug)->first();
        $product = Product::with('images')->where('slug',$name)->first();

        return view('prodotto')->with('category',$category)->with('product',$product);
    }

}
