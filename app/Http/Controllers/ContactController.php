<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Jobs\ContactEmailJob;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('contatti');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContactFormRequest $request
     * @return Response
     */
    public function send(ContactFormRequest $request)
    {
        //dd($request);
        $this->dispatch(new ContactEmailJob($request));

        \Session::flash('success', 'Messaggio inviato correttamente');

        return redirect()->route('contatti');
    }

}
