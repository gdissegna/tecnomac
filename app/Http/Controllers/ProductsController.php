<?php

namespace App\Http\Controllers;

use App;
use App\CategoryTranslation;
use App\Product;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param $slug
     * @return Response
     */
    public function index($slug)
    {
        $this->translations = new CategoryTranslation();

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $category = Category::with('products')->where('id', $translation->category_id)->first();

        return view('prodotti')->with('category', $category);
    }

    public function download($id)
    {
        $product = Product::findOrFail($id);

        return response()->download($product->pdf);
    }


    public function prodotto($slug,$name)
    {
        $product = Product::with('images')->where('slug',$name)->first();
        $this->translations = new CategoryTranslation();
        $translation = $this->translations->getBySlug($slug);
        if ( ! $translation)
        {
            return App::abort(404);
        }
        $category = Category::with('products')->where('id', $translation->category_id)->first();

        $menu_products = Product::where('category_id', $category->id)->get();

        return view('prodotto')->with('product',$product)->with('category', $category)->with('menu_products', $menu_products);
    }
}
