<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_it' => 'required|min:3',
            'title_en' => 'required|min:3',
            'title_de' => 'required|min:3',
            'title_ru' => 'required|min:3',
            'body_it' => 'required|min:10',
            'body_en' => 'required|min:10',
            'body_de' => 'required|min:10',
            'body_ru' => 'required|min:10',
            'image' => 'required'
        ];
    }
}
