<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoryCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_it' => 'required',
            'name_en' => 'required',
            'name_de' => 'required',
            'name_ru' => 'required',
            'description_it' => 'required',
            'description_en' => 'required',
            'description_de' => 'required',
            'description_ru' => 'required',
            'image' => 'required|image'
        ];
    }
}
