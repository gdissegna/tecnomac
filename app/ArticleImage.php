<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
    protected $fillable = ['path', 'th_path'];

    public function Article()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
}
