<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model implements SluggableInterface
{
    use SluggableTrait;

    public $timestamps = false;
    protected $fillable = ['title','description','slug'];

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to' => 'slug',
    );

    public function offer() {
        return $this->belongsTo('Offer','offer_id');
    }

    public function getBySlug($slug) {

        return $this->where('slug', '=', $slug)->first();
    }


}
