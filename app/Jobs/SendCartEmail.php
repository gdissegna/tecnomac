<?php

namespace App\Jobs;

use Mail;
use App\Jobs\Job;
use App\Http\Requests\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class SendCartEmail extends Job implements SelfHandling
{
    public function __construct(Request $request,$cart)
    {
        $this->request = $request;
        $this->cart = $cart;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('mail.cart', ['request' => $this->request,'cart' => $this->cart], function($message) {
            $message->from('info@tecnomecsrl.it', 'Tecnomec');
            $message->to($this->request['email'], $this->request['name'] . $this->request['cognome'] );
            $message->subject('TECNOMEC,' . trans('custom.cart_subject'));
        });

        Mail::send('mail.cartinternal', ['request' => $this->request,'cart' => $this->cart], function($message) {
            $message->from('info@tecnomecsrl.it', 'Tecnomec');
            $message->to('info@tecnomecsrl.it');
            $message->subject('TECNOMEC nuovo carrello');
        });

    }
}
