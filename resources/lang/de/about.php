<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'Das Unterhnemen',
    'titolo'     => 'Tecnomec Srl - Italienisch Leader in der Herstellung von Werkzeugen für die Arbeits für die Holzbearbeitung.',
    'testo' => 'Die Firma <b>Tecnomec</b>, die Werkzeuge für Holzbearbeitung herstellt, ist 1979 gegründet worden, um den Bedürfnissen der vielen Industrien des berühmten "Triangolo della                     Sedia" entgegenzukommen. Während der vergangenen Jahre hat sie sich immer mehr vergrössert. Zu Zeit arbeiten 15 Personen in einer Werkstätte von 2500 mq Grösse. Es                       werden Fräsen, Bohrer, Wendeplatten von Typ HS-HW Diamant produziert. Diese Firma verfügt über ein eigenes Konstruktionsbüro und ist deswegen in der Lage, nach kurzer                   Zeit jedes Problem zu lösen, sowohl der Holz - als auch der Spanplattenbe. Die Firma <b>Tecnomec</b> verwendet nur die besten Stoffe, die heutzutage auf dem Markt finden kann.                 Aus diesem Grund ist sie bekannt und geschätzt in Italien und im Ausland. Um ihre Produkte zu verkaufen, bedient sie sich eines Netzes von Verkäufern und von                             Konzessionären, die auf der ganzen Welt verteilt sind.',
         
   

];