<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'logo'     => 'Logo',
    
    
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'Tecnomec Srl - Italienisch Leader in der Herstellung von Werkzeugen für die Arbeits für die Holzbearbeitung',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   
    
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'Unternehmen',
    'notizie' => 'News',
    'prodotti' => 'Produkte',
    'diamante' => 'Dia Werkzeuge',
    'coltellino' => 'Wendeplatten Werkzeuge',
    'widia' => 'Widia Werkzeuge',
    'contatti' => 'Kontakt',
    'carrello' => 'Preiszusammenfassung',
    'newprod2021-text' => 'We are proud to present our new 2021 products',
    'newprod-text' => 'We are proud to present our new 2020 products',
    'newprod-desc' => 'SET TENONING CNC MACHINES Ø30/48 ATT.20X50<br>ADJUSTABLE 10÷30',
    
     /*
   |--------------------------------------------------------------------------
   | Newsletter
   |--------------------------------------------------------------------------
   */

    'iscrizione' => 'Newsletter abonnieren',
    'iscrizione_button' => 'Registrieren',
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */

    
    'claim' => 'Die Lösung zu jedes Problem',
    'vai' => 'Mehr',
    'ultime_notizie' => 'News',
    'leggi_tutto' => 'Lesen Sie',
    'inevidenza' => 'Wightigste Produkte',
    //'offerte' => 'Angeboten',
    'offerte' => 'News',

     /*
   |--------------------------------------------------------------------------
   | Prodotti
   |--------------------------------------------------------------------------
   */

    'title_articolo' => 'Artikel',
    'download_catalogo' => 'Download Katalog 2023',
    'download_file' => 'Download file',
    'link_utili' => 'Nützliche Links',
    'carrello_button' => 'In den Warenkorb',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
    
    /*
   |--------------------------------------------------------------------------
   | Contatti
   |--------------------------------------------------------------------------
   */

    'contatti_title' => 'Kontakt',
    'contatti_form_title' => 'Informationsanfrage',
    'contatti_form_header' => 'Sollten Sie Informationen brauchen, füllen Sie bitte das hier eingetragene Formular aus: unser Team wird sich sofort mit Ihnen in Verbindung setzen.
                             Die Felder, die mit einem Sternchen (*) gekennzeichnet sind müssen ausgefüllt werden. ',
    'contatti_subtitle' => 'Sprechen oder schreiben Sie uns an, unser Team steht Ihnen zu Verfügung',
    'contatti_indirizzo' => 'Adresse',
    'contatti_contatti' => 'Kontakt',
    'contatti_telefono' => 'Telefon',
    'label_nome' => 'Vorname und Name',
    'label_societa' => 'Gesellschaft',
    'label_indirizzo' => 'Adresse',
    'label_citta' => 'Stadt',
    'label_paese' => 'Land',
    'label_telefono' => 'Telefon',
    'label_fax' => 'Fax',
    'label_email' => 'Email',
    'label_attivita' => 'Aktivität',
    'label_rivenditore' => 'Grosshändler',
    'label_utilizzatore' => 'Benutzer',
    'label_costruttore' => 'Produzent',
    'label_messaggio' => 'Bemerkung',
    'label_privacy' => 'Privacy',
    'label_privacy_testo' => 'Mit Bezug auf die italienische Gesetzverordnung 196/2003 (Schutzmaßnahme der Personalangaben) teilen wir Ihnen mit, dass Ihre Personalangaben nur zwecks                                 Internetdienst (Besuch der Webseite, Informationsanfrage, Registrierung für Dienstleistungen) und zwecks Zusendung unserer Verkaufsunterlagen dienen. Der                               Datenschutzbeauftragte ist Tecnomec S.r.l. Sie können Ihre laut Art.7 der italienischen Gesetzverordnung 196/2003 vorgesehenen Rechte ausüben indem Sie                                 sich wenden an: Tecnomec S.r.l., P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Tel. +39.0481.676679, Fax +39.0481.676681, E-mail:                                   info@tecnomecsrl.it..',
    'label_privacy_ok' => 'Ich bin damit einverstanden, dass sie meine Personalangaben für die o.g. Zwecke bearbeiten. *',
    'label_invia' => 'Absenden',
    
    /*
   |--------------------------------------------------------------------------
   | FORM EMAIL
   |--------------------------------------------------------------------------
   */
    
    'contact_subject' => 'Vielen Dank für Ihre Kontaktaufnahme',
    'cart_subject' => 'Artikel im Warenkorb',
    'contact_title' => 'Zusammenfassung Information',
    'carrello_prodotti' => 'Artikel im Warenkorb',
    'rimuovi_carrello' => 'Herausnehmen',
       
    
    /*
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'telefono' => 'Telefon',
    'indirizzo' => 'Addresse',
    'copyright' => 'Copyright',
    'notelegali' => 'Vorschriften',
    'privacy' => 'Privacy',
    'email_adm_txt' => 'Für administrative Anfragen',
       
   

];