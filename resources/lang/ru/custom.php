<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'logo'     => 'Logo',
    
    
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'Tecnomec Srl - ',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   
    
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'ПРЕДПРИЯТИЕ',
    'notizie' => 'НОВОСТИ',
    'prodotti' => 'ПРОДУКЦИИ',
    'diamante' => 'DIA tools',
    'coltellino' => 'Knives cutterheads',
    'widia' => 'Widia cutterheads',
    'contatti' => 'КОНТАКТЫ',
    'carrello' => 'Запрос предложения',
    'newprod2021-text' => 'We are proud to present our new 2021 products',
    'newprod-text' => 'We are proud to present our new 2020 products',
    'newprod-desc' => 'SET TENONING CNC MACHINES Ø30/48 ATT.20X50<br>ADJUSTABLE 10÷30',
    
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */

    'claim' => 'Решение любой проблемы',
    'vai' => 'Гуарда',
    'ultime_notizie' => 'НОВОСТИ',
    'leggi_tutto' => 'подробнее',
    'inevidenza' => 'ПОПУЛЯРНЫЕ МОДЕЛИ',
    //'offerte' => 'предложения',
    'offerte' => 'НОВОСТИ',

      /*
   |--------------------------------------------------------------------------
   | Prodotti
   |--------------------------------------------------------------------------
   */

    'title_articolo' => 'Артикли',
    'download_catalogo' => 'Скачать каталог 2023',
    'download_file' => 'Скачать file',
    'link_utili' => 'Полезные ссылки',
    'carrello_button' => 'Добавить в корзину',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
    
    /*
   |--------------------------------------------------------------------------
   | Contatti
   |--------------------------------------------------------------------------
   */

    'contatti_title' => 'КОНТАКТЫ',
    'contatti_form_title' => 'Запрос информации',
    'contatti_form_header' => 'Для получения любой интересующей вас информации заполните, пожалуйста, размещенную здесь анкету, наши сотрудники свяжутся с вами и предоставят всю                                       необходимую информацию. Пункты, отмеченные «звездочкой» обязательны для заполнения. ',
    'contatti_subtitle' => 'Пишите, звоните нам, наш персонал всегда к вашим услугам',
    'contatti_indirizzo' => 'Адрес',
    'contatti_contatti' => 'КОНТАКТЫ',
    'contatti_telefono' => 'Телефон',
    'label_nome' => 'Имя и фамилия',
    'label_societa' => 'Фирма',
    'label_indirizzo' => 'Адрес',
    'label_citta' => 'Город',
    'label_paese' => 'Страна',
    'label_telefono' => 'Телефон',
    'label_fax' => 'Факс',
    'label_email' => 'Email',
    'label_attivita' => 'Вид деятельности',
    'label_rivenditore' => 'Сеть розничной торговли',
    'label_utilizzatore' => 'пользователь',
    'label_costruttore' => 'строитель',
    'label_messaggio' => 'Текст письма',
    'label_privacy' => 'Заявление о конфиденциальности',
    'label_privacy_testo' => 'В соответствии с законодательным постановлением № 196/2003 (Положение о защите персональных данных), сообщаем Вам, что использование ваших персональных                                 данных связано исключительно с предоставлением услуг сайта (навигация на сайте, запрос информации, регистрация для получения услуг) и с рассылкой наших                                 информационно-рекламных материалов. Владельцем системы по обработке данных является Tecnomec S.r.l. По всем вопросам соблюдения прав, что следует из                                     статьи 7 законодательного постановления 196/2003, Вы можете обращаться в: Tecnomec S.r.l. P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons                                   (GO), Телефон. +39.0481.676679, Факс +39.0481.676681, E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'Подтверждаю мое согласие на использование данных для вышеописанных целей. *',
    'label_invia' => 'Отправить',
    
    /*
   |--------------------------------------------------------------------------
   | FORM EMAIL
   |--------------------------------------------------------------------------
   */
    
    'contact_subject' => 'Благодарим Вас за обращение к нам',
    'cart_subject' => 'сводный перечень',    
    'contact_title' => 'Основная информация',
    'carrello_prodotti' => 'Мои предложения',
    'rimuovi_carrello' => 'удалять',
   
    
    /*
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'telefono' => 'Телефон',
    'indirizzo' => 'Адрес',
    'copyright' => 'Copyright',
    'notelegali' => 'включая код плательщика НДС',
    'privacy' => 'Заявление о конфиденциальности',
    'email_adm_txt' => 'Для административных запросов',
       
   

];