<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'privacy_titolo'     => 'PRIVACY',
    'privacy_subtitle' => 'Personal data protection code',
    'privacy_testo' => '<p>Tecnomec S.r.l. has the pleasure to welcome you to its website. Before consulting the website, please read attentively this page..</p>
                        <p>PERSONAL DATA PROTECTION CODE</p>
                        <p>According to the Legislative Decree n. 196/2003 (personal data protection code), we inform you that your personal data will be processed with the purpose of providing the services required (website surfing, information request, sign-in for services) and of sending advertising and promotion information and/or material. We also inform you that, in order to handle your request, your personal data may be communicated to external subjects performing activities connected to and functional to the service (such as information and computer system management).</p>
                        <p>Those functions are crucial to execute these operations.</p>
                        <p>TECNOMEC S.r.l. IS RESPONSIBLE FOR THE DATA PROCESSING.</p>
                        <p>You may exercise the rights provided by art.7 of Legislative Decree n. 196/2003 by writing to::</p>
                        <p>TECNOMEC S.r.l.<br />
                        Via Aquileia, 58 (SS305)<br />
                        34071 Cormons (GO)<br />
                        Italy</p>
                        <p>I hereby grant my consent to the processing of my personal data according to the above stated purposes.</p>',     
   

];