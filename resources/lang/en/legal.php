<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'legal_titolo'     => 'NOTE LEGALI E DATI SOCIETARI',
    'legal_subtitle' => 'Avvertenze sulle informazioni relative a prodotti e servizi',
    'legal_testo' => '<p>Le informazioni presenti sul sito www.tecnomecsrl.it hanno natura di messaggio pubblicitario con finalità promozionale. I colori, le immagini, le descrizione, le misure dei prodotti presentati sono esclusivamente illustrativi e non costituiscono attestazione o garanzia delle effettive caratteristiche dei modelli in vendita.</p>
                        <p>Copyright: I marchi, i modelli, i brevetti, i testi, le immagini, il materiale visuale, il software, presenti nel sito, sono di proprietà della Tecnomec S.r.l. o comunque da questa usati col consenso dei titolari.</p>
                        <p>RECEPIMENTO ART.42, L. 7.7.2009, N. 88 (COMUNITARIA 2008)</p>
                        <p>In base ai nuovi obblighi di pubblicità sul web introdotti dall\'articolo di legge di cui sopra, rendiamo noti i seguenti dati societari:</p>
                        <p>TECNOMEC S.r.l.<br />
                        Via Aquileia, 58 (SS305)<br />
                        34071 Cormons (GO)<br />
                        Italy<br />
                        Reg.impr. C.F. P.IVA 00160670303<br />
                        Numero d’identificazione CEE IT00953430303<br />
                        Tel. ++39.0481.676679<br />
                        Fax. ++39.0481.676681</p>
                        <p>Il testo integrale della Legge 88/2009 è disponibile per la consultazione al seguente link: <a href="www.parlamento.it/parlam/leggi/messaggi/c2320a.pdf" target="_blank">www.parlamento.it/parlam/leggi/messaggi/c2320a.pdf</a></p>',     
   

];