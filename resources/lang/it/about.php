<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'La nostra Azienda',
    'titolo'     => 'Tecnomec Srl - leader italiano nella produzione di utensili per la lavorazione del legno.',
        'testo' => '<b>Tecnomec Srl</b>, operante dal 1979 sul mercato degli utensili per la lavorazione del legno, rappresenta ad oggi un imprescindibile punto di riferimento per gli                             operatori del settore. Trasferitasi a gennaio 2004 nei locali della nuova sede operativa, costruita ex novo su una superficie di circa 2200 mq, <b>Tecnomec</b> impiega 15                       addetti nella produzione di frese/punte/teste in HW/HS/DP. L’ufficio di progettazione interno, grazie all’apporto di tecnici altamente qualificati, è in grado di                         risolvere in tempi brevi ogni problematica relativa alla costruzione e al corretto utilizzo del prodotto, offrendo al cliente un prezioso quanto raro servizio                           d’assistenza. Le materie prime impiegate in produzione sono in ogni settore le migliori reperibili sul mercato; ovviamente, il valore aggiunto è dato dal know-how                       della manodopera specializzata, che conferisce agli utensili <b>Tecnomec</b> un livello qualitativo altissimo. Per la vendita, <b>Tecnomec</b> si avvale di una rete di rivenditori                     autorizzati e concessionari dislocati in tutto il mondo.',
         
   

];