<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'privacy_titolo'     => 'PRIVACY',
    'privacy_subtitle' => 'Informativa della Privacy',
    'privacy_testo' => '<p>La Tecnomec S.r.l. Vi dà il benvenuto nel sito web aziendale. Prima di proseguire nella consultazione del sito Vi invita a leggere con attenzione questa                               pagina.</p>
                        <p>CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI</p>
                        <p>In riferimento al D.lgs 196/2003 (codice in materia di protezione dei dati personali), Le comunichiamo che il trattamento dei suoi dati personali è                                   finalizzato alla fornitura del servizio in oggetto (navigazione del sito, richiesta informazioni, registrazione per servizi) ed all\'invio di nostro materiale                           promozionale e/o pubblicitario. Le ricordiamo altresì che, per la gestione della sua richiesta, i suoi dati potranno essere comunicati anche a soggetti esterni                           che svolgono attività connesse e funzionali all\'operatività del servizio (come: gestione del sistema informatico e telematico).</p>
                        <p>Tali funzioni sono fondamentali per l\'esecuzione dell\'operazione.</p>
                        <p>IL TITOLARE DEL TRATTAMENTO DEI DATI È TECNOMEC S.r.l.</p>
                        <p>Lei può esercitare i diritti di cui all\'Art.7 del D.lgs 196/2003 rivolgendosi a:</p>
                        <p>TECNOMEC S.r.l.<br />
                        Via Aquileia, 58 (SS305)<br />
                        34071 Cormons (GO)<br />
                        Italy</p>
                        <p>Acconsento al trattamento dei dati per i fini sopra descritti.</p>',     
   

];