<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'logo'     => 'Logo',
    
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'Tecnomec Srl - leader italiano nella produzione di utensili per la lavorazione del legno',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   
    
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'Azienda',
    'notizie' => 'News',
    'prodotti' => 'Prodotti',
    'diamante' => 'Frese in Diamante',
    'coltellino' => 'Frese a coltellino',
    'widia' => 'Frese in Widia',
    'contatti' => 'Contatti',
    'carrello' => 'Carrello quotazioni',
    'newprod2021-text' => 'Siamo lieti di presentarVi le novità 2021',
    'newprod-text' => 'Siamo lieti di presentarVi le novità 2020',
    'newprod-desc' => 'PUNTA TONONATRICE Ø30/48 ATT.20x50<br>REGOLABILE 10÷30',
    
    /*
   |--------------------------------------------------------------------------
   | Newsletter
   |--------------------------------------------------------------------------
   */

    'iscrizione' => 'Iscriviti alla Newsletter',
    'iscrizione_button' => 'Iscriviti',
    
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */

    'claim' => 'La soluzione ad ogni problema - The solution to all your needs - Решение любой проблемы',
    'vai' => 'Vai',
    'ultime_notizie' => 'Ultime notizie',
    'leggi_tutto' => 'Leggi tutto',
    'inevidenza' => 'Prodotti in evidenza',
    //'offerte' => 'Offerte',
    'offerte' => 'Ultime notizie',

    
    /*
   |--------------------------------------------------------------------------
   | Prodotti
   |--------------------------------------------------------------------------
   */

    'title_articolo' => 'Articolo',
    'download_catalogo' => 'Download catalogo 2023',
    'download_file' => 'Download file',
    'link_utili' => 'Link utili',
    'carrello_button' => 'Aggiungi al carrello',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
    
    
    
    /*
   |--------------------------------------------------------------------------
   | Contatti
   |--------------------------------------------------------------------------
   */

    'contatti_title' => 'Contattateci',
    'contatti_form_title' => 'Richiesta informazioni',
    'contatti_form_header' => 'Per ogni informazione a cui sei interessato, compila il modulo che vedi qui riportato: il nostro Staff ti contatterà per fornirti ogni indicazione                                       necessaria. I campi contrassegnati con un asterisco *, sono obbligatori. ',
    'contatti_subtitle' => 'Scriveteci, telefonateci: il nostro Staff per Voi',
    'contatti_indirizzo' => 'Indirizzo',
    'contatti_contatti' => 'Contatti',
    'contatti_telefono' => 'Telefono',
    'label_nome' => 'Nome e cognome',
    'label_societa' => 'Società',
    'label_indirizzo' => 'Indirizzo',
    'label_citta' => 'Citta',
    'label_paese' => 'Paese',
    'label_telefono' => 'Telefono',
    'label_fax' => 'Fax',
    'label_email' => 'Email',
    'label_attivita' => 'Tipo di attività',
    'label_rivenditore' => 'Rivenditore',
    'label_utilizzatore' => 'Utilizzatore',
    'label_costruttore' => 'Costruttore',
    'label_messaggio' => 'Messaggio',
    'label_privacy' => 'Privacy',
    'label_privacy_testo' => 'In riferimento al D.lgs 30 giugno 2003, n. 196 (codice in materia di protezione dei dati personali), Le comunichiamo che il trattamento dei suoi dati                                   personali è finalizzato alla gestione di questa operazione, all\'invio di nostro materiale informativo e all\'utilizzo dei dati da parte di Tecnomec                                     S.r.l. Il Titolare del trattamento dei dati è Tecnomec S.r.l. Lei può esercitare i diritti di cui all\'Art. 7 del D.lgs 196/03 (tra cui i diritti di                                     accesso, rettifica, aggiornamento e cancellazione) rivolgendosi a Tecnomec S.r.l., P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Tel.                               +39.0481.676679, Fax +39.0481.676681, E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'Do il mio consenso al trattamento dei dati per i fini sopra descritti. *',
    'label_invia' => 'Invia',
    
    /*
   |--------------------------------------------------------------------------
   | FORM EMAIL
   |--------------------------------------------------------------------------
   */
    
    'contact_subject' => 'Grazie per averci contattato',
    'cart_subject' => 'Riepilogo quotazione',
    'contact_title' => 'Riepilogo informazioni',
    'carrello_prodotti' => 'Prodotti nel carrello',
    'rimuovi_carrello' => 'Elimina',
   
    
    /*
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'telefono' => 'Telefono',
    'indirizzo' => 'Indirizzo',
    'copyright' => 'Copyright',
    'notelegali' => 'Note legali',
    'privacy' => 'Privacy Policy',
    'email_adm_txt' => 'Per richieste amministrative',
       
   

];