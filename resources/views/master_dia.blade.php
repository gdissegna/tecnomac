<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8" />
    <title>{{ trans('custom.site-title') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="{{ trans('custom.keywords') }}" />
    <meta name="description" content="{{ trans('custom.description') }}" />
    <meta name="author" content="Omega" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" />
    @include('partials.css')
</head>
<body>{!! Analytics::render() !!}
<div id="page" class="page-wrap">
    <!-- Page Loader -->
    <div id="pageloader">
        <div class="loader-item fa fa-spin text-color"></div>
    </div>
    <div id="top-bar" class="top-bar-section top-bar-bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6 small-12">
                    <form class="form-inline" action="http://d5f9i.s70.it/frontend/subscribe.aspx">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Iscriviti alla newsletter</label>
                            <div class="input-group">
                                <div class="input-group-addon">Iscriviti alla newsletter</div>
                                <input type="email" name="email" class="form-control" id="exampleInputAmount" placeholder="Email">
                            </div>
                        </div>
                        <input type="hidden" name="group" value="58" >
                        <input type="hidden" name="list" value="6">
                        <button type="submit" class="btn btn-primary" style="margin-top: 0px;">Iscriviti</button>
                    </form>
                </div>
                <div class="col-md-6 top-contact link-hover-black" style="float: right;">
                    <ul class="navbar-nav">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" class="button">
                                    <img src="http://test.tecnomecsrl.it/images/flags/flag-{{$localeCode}}.jpg" width="25" height="17" ></img> {{{ $properties['native'] }}}

                                </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-md-6 col-md-offset-3 ">
                        <a href="{{ route('index') }}"><img  class="img-responsive center-block" src="{{ asset('/images/logo-dia.gif') }}"></a>
                    </div>

                    <!-- Top Social Icon -->
                </div>
            </div>
        </div>
    </div>
    <!-- Top Bar -->
    <header class="new-version">
        <div class="container dark-header">
            <div class="row navbar navbar-default" role="navigation">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <!-- Button For Responsive toggle -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span></button>
                    </div>
                    <!-- Navbar Collapse -->
                    <div class="navbar-collapse collapse">
                        <!-- nav -->
                        <ul class="nav navbar-nav full-width">
                            <!-- Home  Mega Menu -->
                            <li>
                                <a class="active" href="{{ route('index') }}">{{ trans('custom.home') }}</a>
                            </li>
                            <!-- Mega Menu Ends -->
                            <!-- Ends Features Menu -->
                            <!-- ABout Us -->
                            <li>
                                <a href="{{ route('azienda') }}">{{ trans('custom.azienda') }}</a>
                            </li>
                            <!-- Ends Widgets Block -->
                            <!-- Service Menu -->
                            <li>
                                <a href="">{{ trans('custom.prodotti') }}</a>
                                <ul class="dropdown-menu">
                                    @foreach($menu_categories as $menu_category)
                                        <li>
                                            <a href="{{ route('categoria',[$menu_category->slug]) }}">{{ $menu_category->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <!-- Service Menu -->
                            <!-- Contact Block -->
                            <li>
                                <a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a>
                            </li>
                            <!-- Ends Contact Block -->
                            <!-- Contact Block -->
                            <li>
                                <a href="{{ route('carrello') }}">{{ trans('custom.carrello') }}</a>
                            </li>
                            <!-- Ends Contact Block -->
                        </ul>
                        <!-- Right nav -->
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </header>
    <!--  Navbar -->

    @yield('content')

    <section id="contact-info" class="contact-info bg-color tb-pad-60 white">
        <div class="container">
            <div class="row">
                <div class="col-md-4 item-box">
                    <i class="icon-phone4 i-7x"></i>
                    <p class="text-uppercase"><strong>{{ trans('custom.telefono') }}</strong></p>
                    <p>+39.0481.676679</p>
                    <p>&nbsp;</a></p>
                </div>
                <div class="col-md-4 item-box">
                    <i class="icon-location2 i-7x"></i>
                    <p class="text-uppercase"><strong>{{ trans('custom.indirizzo') }}</strong></p>
                    <p>Via Aquileia, 58 (SS305)</p>
                    <p>34071 Cormons (GO) Italia</p>
                </div>
                <div class="col-md-4 item-box">
                    <i class="icon-mail10 i-7x"></i>
                    <p class="text-uppercase"><strong>E-mail</strong></p>
                    <p><a href="mailto:support.zozothemes.com">info@tecnomecsrl.it</a></p>
                    <p>&nbsp;</p>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="top-pad-60">
        <!-- footer-top -->
        <div class="copyright">
            <div class="">
                <div class="row text-center">
                    <!-- Copyrights -->
                    <div class="col-md-12">
                        Copyright &copy; Tecnomec S.r.l. - 2015
                        <br />
                        P.IVA e C.F. 00953430303 - Reg. Imp. GO n. 00953430303 - R.e.a. GO n. 67438 - Capitale sociale € 70.200,00 i.v.
                        <br />
                        <!-- Terms Link -->
                        <a href="#">{{ trans('custom.notelegali') }}</a> /
                        <a href="#">{{ trans('custom.privacy') }}</a>
                    </div>
                    <div class="col-md-12 text-center page-scroll gray-bg icons-circle i-3x">
                        <!-- Goto Top -->
                        <a href="#page">
                            <i class="glyphicon glyphicon-arrow-up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bottom -->
    </footer>
    <!-- footer -->


    @include('partials.js')

</div>
</body>