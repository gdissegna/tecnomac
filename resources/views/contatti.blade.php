@extends('master')



@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('custom.contatti_title') }}</h1>
                    <h5>{{ trans('custom.contatti_subtitle') }}</h5>
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="active">{{ trans('custom.contatti') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="contact-us" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <h5 class="title">
                                <i class="icon-address text-color"></i>{{ trans('custom.contatti_indirizzo') }}</h5>
                            <address>Tecnomec S.r.l.
                                <br />Via Aquileia, 58 (SS305)
                                <br />34071 Cormons (GO)
                                <br />Italia
                            </address>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <h5 class="title">
                                <i class="icon-contacts text-color"></i>{{ trans('custom.contatti_contatti') }}</h5>
                            <div>{{ trans('custom.contatti_telefono') }}: +39.0481.676679</div>
                            <div>Fax : +39.0481.676681</div>
                            <div>Email :
                                <a href="mailto:info@tecnomecsrl.it">info@tecnomecsrl.it</a></div>
                        </div>
                    </div>
                    <hr />
                    <p></p>
                </div>
                <div class="col-md-6 col-md-6">
                    <h3 class="title">{{ trans('custom.contatti_form_title') }}</h3>
                    <p class="form-message">{{ trans('custom.contatti_form_header') }}</p>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert-box alert radius">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="contact-form">
                        <!-- Form Begins -->
                        <form role="form" name="contactform" id="contactform" method="post" action="{{ route('contattipost') }}">
                            <input class="hidden" name="_token" type="hidden" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 1 -->
                                    <div class="input-text form-group">
                                        <input value="{{ old('name') }}" type="text" name="name" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_nome') }} *" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 2 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="societa" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_societa') }}" />
                                    </div>
                                </div>
                            </div>
                            <!-- Field 3 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 3 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="indirizzo" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_indirizzo') }} *" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 4 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="citta" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_citta') }} *" />
                                    </div>
                                </div>
                            </div>
                            <!-- Field 5 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 5 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="paese" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_paese') }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 6 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="phone" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_telefono') }} *" />
                                    </div>
                                </div>
                            </div>
                            <!-- Field 7 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 8 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="fax" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_fax') }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 9 -->
                                    <div class="input-email form-group">
                                        <input type="email" name="email" class="input-email form-control"
                                               placeholder="{{ trans('custom.label_email') }} *" />
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Field 10 -->
                            <div class="form-group">
                                <select name="attivita" class="form-control" >
                                    <option value="" disabled selected>{{ trans('custom.label_attivita') }}</option>
                                    <option>{{ trans('custom.label_rivenditore') }}</option>
                                    <option>{{ trans('custom.label_utilizzatore') }}</option>
                                    <option>{{ trans('custom.label_costruttore') }}</option>
                                </select>
                            </div>
                            <!-- Field 11 -->
                            <div class="textarea-message form-group">
                                    <textarea name="message" class="textarea-message form-control" placeholder="{{ trans('custom.label_messaggio') }}"
                                              rows="6"></textarea>
                            </div>
                             <!-- Field 12 -->
                            <div class="form-group">
                                <p>{{ trans('custom.label_privacy_testo') }}</p>
                            </div>
                        <!-- Field 13 -->
                            <div class="form-group">
                                <input type="checkbox" name="privacy"> {{ trans('custom.label_privacy_ok') }}
                            </div>

                            <div class="form-group">
                                {!! Recaptcha::render([ 'lang' =>  LaravelLocalization::getCurrentLocale() ]) !!}
                            </div>
                        
                            <!-- Button -->
                            <button class="btn btn-default" type="submit">{{ trans('custom.label_invia') }}
                                <i class="icon-paper-plane"></i></button>
                            
                        </form>
                        <!-- Form Ends -->
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- page-section -->
    <section id="map">
        <div class="map-section">
            <div class="map-canvas" data-zoom="12" data-lat="45.943053" data-lng="13.444425" data-type="roadmap"
                 data-title="Tecnomec"
                 data-content="Tecnomec srl&lt;br&gt; Contact: +39 0481.676679&lt;br&gt; &lt;a href=&#39;mailto:info@tecnomecsrl.it&#39;&gt;info@tecnomecsrl.it&lt;/a&gt;"
                 style="height: 376px;"></div>
        </div>
    </section>
    <!-- map -->
@endsection