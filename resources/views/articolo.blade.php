@extends('master')


@section('content')
    <!-- Sticky Navbar -->
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ $article->title }}</h1>

                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('newslist') }}">News</a>
                        </li>
                        <li class="active">{{ $article->slug }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bottom-pad-30">
                    <div class="post-image opacity">
                        <img src="{{ asset($article->primary_img) }}" width="1170" alt="" title="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="post-content">
                                <p>
                                    {{ $article->body }}
                                </p>
                            </div>
                            <div class="post-meta">
                                <!-- Meta Date -->

                                <span class="time">
                                <i class="fa fa-calendar"></i> {{ $article->created_at }}</span>
                                <!-- Comments -->
                            </div>
                        </div>
                    </div>
                </div>


                <div class="sidebar col-sm-12 col-md-3">
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">{{ trans('custom.home') }}</h3>
                        </div>
                        <div id="MainMenu2">
                            <div class="list-group panel">
                                <div class="collapse in" id="demo3">
                                        @foreach ($articles_date as $date => $articles)
                                        <a href="#SubMenu2" class="list-group-item" data-toggle="collapse"
                                           data-parent="#SubMenu2">{{ $date }}
                                            <i class="fa fa-caret-down"></i></a>
                                        <div class="collapse in list-group-submenu" id="SubMenu2">
                                            @foreach ($articles as $article)
                                                <a href="{{ route('singlenews',[$article->slug]) }}" class="list-group-item">{{ $article->title }}</a>
                                            @endforeach
                                        </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- category-list -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page-section -->
@endsection
