<!-- Font -->
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic' />
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />
<!-- Font Awesome Icons -->
<link href='{{ asset('css/font-awesome.min.css') }}' rel='stylesheet' type='text/css' />
<!-- Bootstrap core CSS -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/hover-dropdown-menu.css') }}" rel="stylesheet" />
<!-- Icomoon Icons -->
<link href="{{ asset('css/icons.css') }}" rel="stylesheet" />
<!-- Revolution Slider -->
<link href="{{ asset('css/revolution-slider.css') }}" rel="stylesheet" />
<link href="{{ asset('rs-plugin/css/settings.css') }}" rel="stylesheet" />
<!-- Animations -->
<link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
<!-- Owl Carousel Slider -->
<link href="{{ asset('css/owl/owl.carousel.css') }}" rel="stylesheet" />
<link href="{{ asset('css/owl/owl.theme.css') }}" rel="stylesheet" />
<link href="{{ asset('css/owl/owl.transitions.css') }}" rel="stylesheet" />
<!-- PrettyPhoto Popup -->
<link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet" />
<!-- Custom Style -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
<link href="{{ asset('css/responsive.css') }}" rel="stylesheet" />
<!-- Color Scheme -->
<link href="{{ asset('css/color.css') }}" rel="stylesheet" />



