<!-- Scripts -->
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Menu jQuery plugin -->
<script type="text/javascript" src="{{ asset('js/hover-dropdown-menu.js') }}"></script>
<!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="{{ asset('js/jquery.hover-dropdown-menu-addon.js') }}"></script>
<!-- Scroll Top Menu -->
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
<!-- Sticky Menu -->
<script type="text/javascript" src="{{ asset('js/jquery.sticky.js') }}"></script>
<!-- Bootstrap Validation -->
<script type="text/javascript" src="{{ asset('js/bootstrapValidator.min.js') }}"></script>
<!-- Revolution Slider -->
<script type="text/javascript" src="{{ asset('rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
<!-- Portfolio Filter -->
<script type="text/javascript" src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
<!-- Animations -->
<script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/effect.js') }}"></script>
<!-- Owl Carousel Slider -->
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- Pretty Photo Popup -->
<script type="text/javascript" src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
<!-- Parallax BG -->
<script type="text/javascript" src="{{ asset('js/jquery.parallax-1.1.3.js') }}"></script>
<!-- Fun Factor / Counter -->
<script type="text/javascript" src="{{ asset('js/jquery.countTo.js') }}"></script>
<!-- Twitter Feed -->

<script type="text/javascript" src="{{ asset('js/tweet/carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tweet/scripts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tweet/tweetie.min.js') }}"></script>
<!-- Background Video -->

<script type="text/javascript" src="{{ asset('js/jquery.mb.YTPlayer.js') }}"></script>
<!-- Custom Js Code -->
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<!-- Scripts -->