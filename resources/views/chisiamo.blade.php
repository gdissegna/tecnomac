@extends('master')

@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('about.titolo_azienda') }}</h1>
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="active">{{ trans('custom.azienda') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="about-us" class="page-section">
        <div class="container">
            <h3 class="text-center">{{ trans('about.titolo_azienda') }}</h3>
            <br />
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{{ asset("images/azienda/slider-1.jpg") }}" width="1140" height="570"  alt="" title="" />
                    </div>
                </div>
                <!-- Controls -->
                <div class="row">
                 <div class="col-md-12 content-block">
                     <h1></h1>
                     <p class="text-justify">{!! trans('about.testo') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page-section -->
@endsection