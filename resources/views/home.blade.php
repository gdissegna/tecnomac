@extends('master')

@section('content')
    <section class="slider border-bottom line tp-banner-fullscreen-container">
        <div class="tp-banner-fullscreen1">
            <ul>
                @foreach($sliders as $slider)
                    <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                        <img src="{{ $slider->path }}" alt="" data-bgfit="cover" data-bgposition="center top"
                             data-bgrepeat="no-repeat"/>
                    </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </section>
    <!-- slider -->
    <div id="request-quote" class="request-quote black text-center">
        <div class="container dark-bg white tb-pad-30">
            <div class="row">
                <div class="col-md-12 item-box">
                    <h4>{{ trans('custom.claim') }}</h4>
                </div>
            </div>
        </div>
    </div>

    <!-- request quote-->
    <div class="page-box-wapper">
        <div class="row">
            <section id="services" class="page-section transparent">
                <div class="container">
                    <div class="row">
                        <!--<div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3"
                             data-autoplay="true" data-navigation="true"> -->
                        @foreach($categories as $category)
                            <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInLeft">
                                <p class="text-center">
                                    <a href="{{ $category->image }}" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="{{ $category->image }}" width="420" alt=""/> </a>
                                </p>

                                <h3>
                                    <a href="#">{{ $category->name }}</a>
                                </h3>

                                <a href="{{ route('categoria',['slug' => $category->slug]) }}"
                                   class="btn btn-default">{{ trans('custom.vai') }}</a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </section>
        </div>
    </div>
    
    <!-- prodotto in evidenza 2021 -->
    <section id="welcome" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="heading">{{ trans('custom.newprod2021-text') }}</h1>
                    
                    <br>
                    <br>
                    <img src="http://www.tecnomecsrl.it/images/home/tecnomec-punte-180-181-elic-45.jpg" alt="" height="1158" width="1098">
                </div>
            </div>
        </div>
    </section>
    <!-- fine prodotto in evidenza 2021 -->    
<!-- prodotto in evidenza -->
    <section id="welcome" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="heading">{{ trans('custom.newprod-text') }}</h1>
                    
                    <br>
                    <br>
                    <img src="http://www.tecnomecsrl.it/images/home/tecnomec-new-products-2020.jpg" alt="" height="580" width="1158">
                </div>
            </div>
        </div>
    </section>
    <!-- fine prodotto in evidenza -->
    <!-- Services -->
    <section id="latest-news" class="page-section light-bg border-tb">
        <div class="container">
            <div class="row">
                @if( count($articles) != 0 )
                    <div class="section-heading clearfix">
                        <div class="section-title text-left clearfix">
                            <!-- Title -->
                            <h2 class="title">{{ trans('custom.ultime_notizie') }}</h2>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3"
                     data-autoplay="true" data-navigation="true">
                    @foreach($articles as $article)
                        <div class="col-sm-4 col-md-4 col-xs-12">
                            <p class="text-center">
                                <a href="{{ $article->primary_img }}" class="opacity" data-rel="prettyPhoto[portfolio]">
                                    <img src="{{ $article->primary_img }}" width="420" alt=""/> </a>
                            </p>

                            <h3>
                                <a href="#">{{ $article->title }}</a>
                            </h3>

                            <p>{{ str_limit($article->body, 150) }}</p>
                            <a href="{{ route('singlenews',[$article->slug]) }}"
                               class="read-more">{{ trans('custom.leggi_tutto') }}</a>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>

    </section>
    <!-- news -->
    <section id="buy-now" class="page-section tb-pad-30 bg-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center white">
                    <h2 class="heading">{{ trans('custom.download_catalogo') }}</h2>
                    <a class="btn btn-transparent-white btn-lg"
                       href="{{ asset('files/TECNOMEC-Catalogue-2023.pdf') }}" target="_blank">DOWNLOAD</a>
                </div>
            </div>
        </div>
    </section>
    <!-- buy now -->
    <section id="works" class="page-section transparent">
        <div class="container">
            <div class="section-title text-left">
                <!-- Heading -->
                <h2 class="title">{{ trans('custom.inevidenza') }}</h2>
            </div>
            <!-- filter -->
            <div class="container general-section">
                <!--
                <div id="options" class="filter-menu">
                    <ul class="option-set nav nav-pills">
                        <li class="filter active" data-filter="all">Show All</li>
                        <li class="filter" data-filter=".commercial">Commercial</li>
                        <li class="filter" data-filter=".education">Education</li>
                        <li class="filter" data-filter=".healthcare">Healthcare</li>
                        <li class="filter" data-filter=".residential">Residential</li>
                    </ul>
                </div>
                -->
            </div>

            <div class="container white general-section">
                <div id="mix-container" class="portfolio-grid custom no-item-pad">
                    <!-- Item 1 -->
                    @foreach($products as $product)
                        <div class="grids col-xs-12 col-sm-4 col-md-3 mix all commercial">
                            <h4 class="text-center black">Art.{{ $product->name }}</h4>
                            <div class="grid">
                                <img src="{{ asset($product->primary_img) }}" width="400" height="273" alt="Recent Work"
                                     class="img-responsive"/>
                                <div class="figcaption">
                                    <h4>{{ $product->name }}</h4>
                                    <!-- Image Popup-->
                                    <a href="{{ asset($product->primary_img) }}" data-rel="prettyPhoto[portfolio]"> <i
                                                class="fa fa-search"></i> </a> <a
                                            href="{{  route('prodotto',[ $product->category->slug , $product->slug]) }}">
                                        <i class="fa fa-link"></i> </a>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!-- Item 1 Ends-->

                </div>
            </div>
            <!-- Mix Container -->
        </div>
        <!-- base Container -->
    </section>
    <!-- works -->
    @if( count($offers) != 0 )
        <section id="about-us" class="page-section light-bg no-pad">
            <div class="container">
                <div class="section-title text-left">
                    <h2 class="title">{{ trans('custom.offerte') }}</h2>
                </div>
            </div>
            <div class="container who-we-are">
                @foreach($offers as $offer)
                    <div class="row">
                        <div class="col-md-6 pad-60">
                            <h4>{{ $offer->title }}</h4>

                            <p>
                                {{ $offer->description }}<br>
                            </p>

                            <h3>
                                @if($offer->pdf)
                                    <a href="{{ route('offertadownload',[$offer->id]) }}"
                                       class="hover">{{ trans('custom.download_file') }} - <i
                                                class="icon-file-pdf red"></i></a>
                                @endif
                            </h3>
                        </div>
                        <div class="col-md-6 no-pad text-center">
                            <!-- Image -->
                            <div class="image-bg" data-background="{{ asset($offer->primary_img) }}"></div>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </section>
        @endif</div>
        <!-- who-we-are -->


@endsection

