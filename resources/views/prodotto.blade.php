@extends($product->category_id == 1 ? 'master_dia' : 'master');


@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('custom.title_articolo') }} {{ $product->name }}</h1>
                    
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li>
                            <a href="#">{{ trans('custom.home') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="services" class="page-section service-section">
        <div class="container">
                <div class="col-sm-12 col-md-9 col-md-push-3">
                    <div class="row">
                        <div class="col-md-12 content-block">
                            @if( Session::has('product_added') )
                                <p class="alert alert-info">{{ Session::get('product_added') }}</p>
                            @endif
                            <div class="text-center">
                                <div class="owl-carousel navigation-4" data-pagination="false" data-items="1"
                                     data-singleitem="true" data-autoplay="true" data-navigation="true">
                                    <a href="{{ asset($product->primary_img) }}" class="opacity" data-rel="prettyPhoto[prodotto]">
                                        <img src="{{ asset($product->primary_img) }}" width="1000" height="500" alt="" />
                                    </a>
                                    </div>
                            </div>
                            <p>
                                {{ $product->description }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                       @foreach( $product->images as $image)
                        <div class="col-md-6 content-block opacity">
                            <a href="{{ asset($image->image) }}" class="opacity" data-rel="prettyPhoto[prodotto]">
                                <img src="{{ asset($image->image) }}" width="1000" height="500" alt="" />
                            </a>
                        </div>
                        @endforeach
                           <div class="col-md-12 content-block">
                               {!! $product->video !!}
                           </div>
                    </div>
                    <hr class="top-margin-0" />
                    <div class="row">
                        <div class="col-md-6 service-list content-block">
                            <h4>{{ trans('custom.link_utili') }}</h4>
                            <ul>
                                @if($product->pdf)
                                <li>
                                    <i class="icon-file-pdf text-color"></i>
                                    <p><a href="{{ route('prodottoDownload',[$product->id])}}" target="_blank">{{ trans('custom.download_file') }}</a></p>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6 service-list content-block">
                            <a role="button" href="{{ route('carrelloAddItem',[$product->id]) }}" class="button btn btn-default right">{{ trans('custom.carrello_button') }}</a>
                        </div>
                        </div>
                    </div>

                <div class="sidebar col-sm-12 col-md-3 col-md-pull-9">
                    <div class="widget list-border">
                        <div class="widget-title">
                            <h3 class="title">{{ trans('custom.prodotti') }}</h3>
                        </div>
                        <div id="MainMenu">
                            <div class="list-group panel">
                                <a href="#SubMenu1" class="list-group-item" data-toggle="collapse"
                                   data-parent="#MainMenu">{{ $category->name }}</a>
                                <div class="collapse in list-group-submenu list-group-submenu-2" id="SubMenu1">
                                    @foreach($menu_products as $menu_product)
                                        @if($menu_product->id === $product->id)
                                            <a href="{{route('prodotto',['slug' => $menu_product->category->slug, 'name' => $menu_product->slug ]) }}" class="list-group-item active" data-parent="#SubMenu1">{{ $menu_product->name }}</a>
                                        @else
                                            <a href="{{ route('prodotto',['slug' => $menu_product->category->slug, 'name' => $menu_product->slug ]) }}" class="list-group-item" data-parent="#SubMenu1">{{ $menu_product->name }}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- category-list -->
                    </div>
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">Download Brochures</h3>
                            <div id="MainMenu2">
                                <div class="list-group list-icons panel">
                                    <div class="collapse in" id="demo3">
                                        <a href="{{ asset('files/TECNOMEC-Catalogue-2023.pdf') }}" class="list-group-item pdf" target="_blank">{{ trans('custom.download_catalogo') }}                                         <i class="fa fa-file-pdf-o"></i></a>
                                    </div>
                                </div>
                                <!-- category-list -->
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
    <!-- Services -->
@endsection

