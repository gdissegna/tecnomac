@extends('master')


@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('custom.carrello') }}</h1>
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="active">{{ trans('custom.carrello') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="contact-us" class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <h5 class="title">
                                <i class="icon-address text-color"></i>{{ trans('custom.contatti_indirizzo') }}</h5>
                            <address>Tecnomec S.r.l.
                                <br />Via Aquileia, 58 (SS305)
                                <br />34071 Cormons (GO)
                                <br />Italia
                            </address>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <h5 class="title">
                                <i class="icon-contacts text-color"></i>{{ trans('custom.contatti_contatti') }}</h5>
                            <div>Phone : +39.0481.676679</div>
                            <div>Fax : +39.0481.676681</div>
                            <div>Email :
                                <a href="mailto:info@tecnomecsrl.it">info@tecnomecsrl.it</a></div>
                        </div>
                    </div>
                    <hr/>
                    <h1>{{ trans('custom.carrello_prodotti') }}</h1>

                    <div class="row text-center top-pad-30">
                        @foreach($cartitems as $product)
                            <div class="col-sm-4 col-md-4 opacity">
                                <a href="{{ asset($product->primary_img) }}" class="opacity"
                                   data-rel="prettyPhoto[products]">
                                    <img src="{{ asset($product->primary_img) }}" width="370" alt=""/>
                                </a>

                                <h3>{{ $product->name }}</h3>
                                <a href="{{ route('carrelloRemoveItem',[$product->id]) }}"
                                   class="read-more">{{ trans('custom.rimuovi_carrello') }}</a>
                                <a href="{{ route('prodotto',[$product->category->slug , $product->slug]) }}"
                                   class="read-more">{{ trans('custom.leggi_tutto') }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 col-md-6">
                    <h3 class="title">{{ trans('custom.contatti_form_title') }}</h3>
                    <p class="form-message">{{ trans('custom.contatti_form_header') }}</p>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert-box alert radius">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="contact-form">
                        <!-- Form Begins -->
                        <form role="form" name="contactform" id="contactform" method="post"
                              action="{{ route('carrellopost') }}">
                            <input class="hidden" name="_token" type="hidden" value="{{ csrf_token() }}">

                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 1 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="name" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_nome') }} *"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 2 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="societa" class="input-email form-control"
                                               placeholder="{{ trans('custom.label_societa') }}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Field 3 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 3 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="indirizzo" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_indirizzo') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 4 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="citta" class="input-email form-control"
                                               placeholder="{{ trans('custom.label_citta') }}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Field 5 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 5 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="paese" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_paese') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 6 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="phone" class="input-email form-control"
                                               placeholder="{{ trans('custom.label_telefono') }}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Field 7 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Field 8 -->
                                    <div class="input-text form-group">
                                        <input type="text" name="fax" class="input-name form-control"
                                               placeholder="{{ trans('custom.label_fax') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Field 9 -->
                                    <div class="input-email form-group">
                                        <input type="email" name="email" class="input-email form-control"
                                               placeholder="{{ trans('custom.label_email') }}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Field 10 -->
                            <div class="form-group">
                                <select name="attivita" class="form-control"/>
                                <option value="" disabled selected>{{ trans('custom.label_attivita') }}</option>
                                <option>{{ trans('custom.label_rivenditore') }}</option>
                                <option>{{ trans('custom.label_utilizzatore') }}</option>
                                <option>{{ trans('custom.label_costruttore') }}</option>
                                </select>

                            </div>
                            <!-- Field 11 -->
                            <div class="textarea-message form-group">
                                    <textarea name="message" class="textarea-message form-control"
                                              placeholder="{{ trans('custom.label_messaggio') }}"
                                              rows="6"></textarea>
                            </div>
                            <!-- Field 12 -->
                            <div class="form-group">
                                <p>{{ trans('custom.label_privacy_testo') }}</p>
                            </div>
                            <!-- Field 13 -->
                            <div class="form-group">
                                <input type="checkbox" name="privacy"> {{ trans('custom.label_privacy_ok') }}
                            </div>

                            <div class="form-group">
                                {!! Recaptcha::render([ 'lang' =>  LaravelLocalization::getCurrentLocale() ]) !!}
                            </div>

                            <!-- Button -->
                            <button class="btn btn-default" type="submit">{{ trans('custom.label_invia') }}
                                <i class="icon-paper-plane"></i></button>

                        </form>
                        <!-- Form Ends -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page-section -->

@endsection