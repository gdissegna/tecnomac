@extends('admin.master')

@section('content')
    <div class="large-9 columns">
        <h1>Inserisci nuovo articolo</h1>
        <hr>
        <div class="row">
            <div class="large-9 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <hr>
            <div class="row">
                <div class="large-12 columns">
                    {!! Form::open(['route' => 'ArticleCreatePost', 'method' => 'post','files' => true]) !!}
                    <div class="row">
                        <label>Titolo IT
                            {!! Form::text('title_it', 'titolo news') !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Titolo EN
                            {!! Form::text('title_en', 'titolo news inglese') !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Titolo DE
                            {!! Form::text('title_de', 'titolo news tedesco') !!}
                        </label>
                    </div>
                    <div class="row ">
                        <label>Titolo RU
                            {!! Form::text('title_ru', 'titolo news russo') !!}
                        </label>
                    </div>
                    <div class="row large-collapse">
                        <label>Corpo IT
                            {!! Form::textarea('body_it','',['class' => 'tinymce']) !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Corpo EN
                            {!! Form::textarea('body_en','',['class' => 'tinymce']) !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Corpo DE
                            {!! Form::textarea('body_de', '',['class' => 'tinymce']) !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Corpo RU
                            {!! Form::textarea('body_ru', '',['class' => 'tinymce']) !!}
                        </label>
                    </div>
                    <div class="row">
                        <label>Inserisci immagine
                            {!! Form::file('image') !!}
                        </label>
                    </div>
                    <div class="large-12">
                        {!! Form::submit('Submit', ['class' => 'button']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <br>
                <br>
                <br>
            </div>
    </div>
@endsection