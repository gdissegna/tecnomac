@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div>
            <h1>Inserisci nuova categoria</h1>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            {!! Form::open(['route' => 'CategoryCreatePost', 'method' => 'post', 'files' => true]) !!}

                <div class="large-12 columns">
                <label>Nome IT
                    {!! Form::text('name_it', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Nome EN
                    {!! Form::text('name_en', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Nome DE
                    {!! Form::text('name_de', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Nome RU
                    {!! Form::text('name_ru', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Descrizione IT
                    {!! Form::text('description_it', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Descrizione EN
                    {!! Form::text('description_en', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Descrizione EN
                    {!! Form::text('description_de', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Descrizione EN
                    {!! Form::text('description_ru', '') !!}
                </label>
                </div>


                <div class="large-12 columns">
                <label>Caricamento immagine
                    {!! Form::file('image') !!}
                </label>
                </div>


                <div class="large-12">
                    {!! Form::submit('Submit', ['class' => 'button']) !!}
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection