<html>
<head>
    <title>Tecnomac CMS</title>
    @include('admin.partial.styles')
    @include('tinymce::tpl')
</head>
<body>

<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
        <nav class="tab-bar">
            <section class="left-small">
                <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
            </section>

            <section class="middle tab-bar-section">
                <h1 class="title">Tecnomec CMS</h1>
            </section>
        </nav>
        @include('admin.partial.mobile_sidebar')
        <div class="row">
            @include('admin.partial.sidebar')
            @yield('content')
        </div>
    </div>

</div>
    <div id="footer"></div>

    @include('admin.partial.scripts')

</body>
</html>