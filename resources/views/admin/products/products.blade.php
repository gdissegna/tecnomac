@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1 class="">Lista prodotti</h1>
        </div>
        <div class="large-4 medium-4 columns"><a role="button" href="{{ route('ProductCreate') }}" class="button right">Aggiungi prodotto</a></div>
        <hr>
        <p></p>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>Nome prodotto</th>
                <th width="200">aggiunto il:</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->created_at }}</td>
                <td><ul class="button-group">
                        <li><a href="{{ route('ProductImages', ['id' => $product->id ]) }}" class="button small">immagini</a></li>
                        <li><a href="{{ route('ProductEdit',['id' => $product->id ]) }}" class="button small">edita</a></li>
                        <li><a href="{{ route('ProductDelete',['id' => $product->id ]) }}" class="button small">Cancella</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection