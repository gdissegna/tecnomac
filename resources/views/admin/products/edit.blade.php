@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="row">
            <h1>Edita il Prodotto</h1>
            <hr>
            <div class="large-12">
                @if (count($errors) > 0)
                    <div>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><div class="alert-box alert" data-alert=""> {{ $error }}</div></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['route' => ['ProductEditPost', $product->id ], 'method' => 'post', 'files' => 'true']) !!}
            <div class="large-12">
                <label>Nome prodotto
                    {!! Form::text('name', $product->name) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione IT
                    {!! Form::text('description_it', $product->translate('it')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione EN
                    {!! Form::text('description_en', $product->translate('en')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione DE
                    {!! Form::text('description_de', $product->translate('de')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione RU
                    {!! Form::text('description_ru', $product->translate('ru')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Seleziona una Categoria
                    <select name="category">
                        @foreach($categories as $category)
                            @if($product->category_id === $category->id )
                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                          @endforeach
                    </select>
                </label>
            </div>
            <div class="large-12">
                <label>Edita il frame Video
                    {!! Form::text('video', 'Video') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Modifica immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Modifica file Pdf
                    {!! Form::file('pdf') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Spunta se in evidenza</label>
                {!! Form::checkbox('inhome') !!}
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>

            {!! Form::close() !!}


        </div>
    </div>
@endsection