@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-9 columns">
            <h1>Edita Immagine Prodotto</h1>
            <hr>
        </div>
        <div class="row">
            <div class="large-9 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            {!! Form::open(['route' => ['ProducImagesEditPost', $image->product_id , $image->id ], 'method' => 'post', 'files' => 'true']) !!}

            <div class="large-12 medium-12 columns">
                <label>aggiungi il link
                    {!! Form::text('alttext', '') !!}
                </label>
            </div>
            <div class="large-12 medium-12 columns">
                <label>Aggiungi Immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}


        </div>
    </div>
@endsection