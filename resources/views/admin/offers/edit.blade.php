@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <h1>Edita un Offerta</h1>
        <hr>
        <div class="row">
            <div class="large-9 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            {!! Form::open(['route' => ['OfferEditPost', $offer->id ], 'method' => 'post', 'files' => 'true']) !!}
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('title_it', $offer->translate('it')->title ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('title_en',  $offer->translate('en')->title ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('title_de', $offer->translate('de')->title ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('title_ru', $offer->translate('ru')->title  ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('description_it',  $offer->translate('it')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('description_en',  $offer->translate('en')->description ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('description_de',  $offer->translate('de')->description ) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::text('description_ru',  $offer->translate('ru')->description) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Input Label
                    {!! Form::file('pdf') !!}
                </label>
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}


        </div>
    </div>
@endsection

