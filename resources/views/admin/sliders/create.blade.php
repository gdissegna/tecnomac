@extends('admin.master')


@section('content')
    <div class="large-9 medium-8 columns">
        <h1>Importa una nuova slider</h1>
        <hr>
        {!! Form::open(['route' => 'SliderCreatePost', 'method' => 'post' , 'files' => true]) !!}
            <div class="row">
                <div class="large-12 columns">
                {!! Form::file('file' ) !!}
                <div>
            </div>
            {!! Form::submit('Submit', ['class' => 'button']) !!}
        {!! Form::close() !!}
    </div>
@endsection