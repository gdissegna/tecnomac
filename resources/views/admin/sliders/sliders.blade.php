@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1 class="">Lista slider</h1>
        </div>
        <div class="large-4 medium-4 columns"><a role="button" href="{{ route('SliderCreate') }}" class="button right">Aggiungi Slider</a></div>
        <hr>
        <p></p>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>Immagine</th>
                <th width="200">aggiunta il:</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sliders as $slider)
            <tr>
                <td><img src="{{ $slider->path }}" width="150"></td>
                <td>{{ $slider->created_at }}</td>
                <td><ul class="button-group">
                        <li><a href="{{ route('SliderDelete',['id' => $slider->id ]) }}" class="button">Cancella</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection

