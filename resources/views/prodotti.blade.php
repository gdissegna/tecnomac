@extends($category->id == 1 ? 'master_dia' : 'master');

@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ $category->name }}</h1>
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">{{ trans('custom.prodotti') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="about-us" class="page-section">
        <div class="container">
            <h3 class="text-center">{{ trans('custom.prodotti') }}</h3>
            <br />
            <div class="row text-center top-pad-30 columns">
                @foreach($category->products as $product)
                <div class="col-sm-4 col-md-4 opacity " style="margin-bottom: 10px;">
                    <div class="col-md-12 border">
                    <h3>Art. {{ $product->name }}</h3>
                    <a href="{{ asset($product->primary_img) }}" class="opacity" data-rel="prettyPhoto[products]">
                    <img src="{{ asset($product->primary_img) }}" width="370" alt="" />
                    </a>
                    <a href="{{ route('prodotto',[$category->slug , $product->slug]) }}"
                       class="read-more">{{ trans('custom.leggi_tutto') }}</a>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- page-section -->
@endsection